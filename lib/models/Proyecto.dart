import 'package:flutter/material.dart';

class Proyecto{
  String imagen;
  String nombre;
  String descripcion;
  String link;
  Proyecto({@required this.imagen,@required this.nombre,@required this.descripcion, this.link});
}