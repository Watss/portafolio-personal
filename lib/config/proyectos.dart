import 'package:portafolio/config/assets.dart';
import 'package:portafolio/models/Proyecto.dart';

final List<Proyecto> proyectos = [
  Proyecto(
      nombre: 'BasuraVerde',
      imagen: Assets.basuraverde,
      descripcion:
          'Aplicación que mapea los puntos de reciclaje oficiales de la ciudad de Chillán,Chile. Funciona con una api fabricada en typescrypt y está desarrollada con Dart - Flutter',
      link:
          'https://play.google.com/store/apps/details?id=com.adxtechnology.basuraverdechillan&hl=es_CL'),
  Proyecto(
      nombre: 'Intranet Auto Escuela Incap',
      imagen: Assets.intranetincap,
      descripcion:
          'Plataforma administrativa de alumnos, pagos y test online, Desarrollado en PHP - Laravel',
      link: 'https://intranet.autoescuelaincap.cl'),
  Proyecto(
      nombre: 'Comparador De Isapres',
      imagen: Assets.cotizadorisapres,
      descripcion:
          'Plataforma que le permite a los usuarios comprar entre distintos planes de isapres chilenas y elejir la mas conveniente para el.',
      link: 'https://cotizarmejorisapre.cl'),
  Proyecto(
      nombre: 'Generador De Presupuestos',
      imagen: Assets.presupuestos,
      descripcion:
          'Generador de Presupuestos para empresas medianas, que permite generar presupuestos rapidos y automatizados desde un movil o un pc',
      link: null),
  Proyecto(
      nombre: 'Administración Contable',
      imagen: Assets.administracionContable,
      descripcion:
          'Sistema web de administración contable que le permite a una empresa llevar el control de ingresos y gastos por separado de sus proyectos y departamentos',
      link: null),
  Proyecto(
      nombre: 'Adm. Clinica Dental',
      imagen: Assets.clinicasonria,
      descripcion:
          'Sistema web de administración contable que le permite a una empresa llevar el control de ingresos y gastos por separado de sus proyectos y departamentos',
      link: 'https://desarrollo.clinicadentalsonria.cl'),
  Proyecto(
      nombre: 'Administración de Buses y Ventas',
      imagen: Assets.buses,
      descripcion:
          'Sistema web de administración de empresa de buses y venta de pasajes fisicos, con cliente de impresión y multi sucursal',
      link: null),
  Proyecto(
      nombre: 'Sistema de Estacionamientos',
      imagen: Assets.estacionamiento,
      descripcion:
          'Sistema web de administración de Estacionamientos, calculando tarifas y registrando las patentes de los clientes.',
      link: null),
  Proyecto(
      nombre: 'Plataforma de Organización de Facturas',
      imagen: Assets.facturas,
      descripcion:
          'Plataforma web que le permite a los usuarios registrados administrar sus facturas, clasificandolas y etiquetandolas.',
      link: null),
  Proyecto(
      nombre: 'Sistema de Restaurante Hostal',
      imagen: Assets.restaurante,
      descripcion:
          'Sistema de restaurante de un hostal, con menus fijos diarios y especialmente fabricado para usarse en tablets',
      link: null),
];
