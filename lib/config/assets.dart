  
class Assets{
  static const perfil = 'assets/perfil.jpg';

  //PORTAFOLIO
  static const basuraverde = 'assets/works/basuraverde.png';
  static const intranetincap = 'assets/works/intranetincap.png';
  static const cotizadorisapres = 'assets/works/cotizadorisapres.png';
  static const presupuestos = 'assets/works/presupuestos.png';
  static const administracionContable = 'assets/works/contable.png';
  static const clinicasonria = 'assets/works/clinicasonria.png';
  static const buses = 'assets/works/buses.png';
  static const estacionamiento = 'assets/works/estacionamiento.png';
  static const restaurante = 'assets/works/restaurante.png';
  static const facturas = 'assets/works/facturas.png';
}