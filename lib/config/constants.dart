class Constants{
  static const PROFILE_LINKEDIN = 'https://www.linkedin.com/in/';
  static const PROFILE_FACEBOOK = 'https://www.facebook.com/DiegoJulianGonzalezZapata';
  static const PROFILE_GITHUB = 'https://github.com/Watss';
  static const PROFILE_GITLAB = 'https://gitlab.com/Watss';
  static const PROFILE_INSTAGRAM = 'https://www.instagram.com/diego_run_dev';
}