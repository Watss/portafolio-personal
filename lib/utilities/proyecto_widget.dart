import 'package:portafolio/models/Proyecto.dart';
import 'package:flutter/material.dart';
import 'dart:html' as html;

class ProyectoWidget extends StatelessWidget {
  final Proyecto _proyecto;
  final double _bottomPadding;
  ProyectoWidget(this._proyecto,this._bottomPadding );

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Card(
      elevation: 3.0,
        margin: EdgeInsets.fromLTRB(16.0,16.0,16.0,_bottomPadding),
        child:InkWell(
          onTap: onProjectClick,
          child:  Padding(
          padding: const EdgeInsets.all(8.0),
          child:  Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                  flex: 40,
                  child:  Image.asset(
                    _proyecto.imagen,
                    width: width * .25,
                    height: width*.25,
                  )),
              Expanded(
                flex: 3,
                child: Container(),
              ),
              Expanded(
                flex: 60,
                child: Container(
                  padding: EdgeInsets.only(top: 8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(_proyecto.nombre,
                          style: Theme.of(context).textTheme.headline6),
                      SizedBox(
                        height: height * .01,
                      ),
                      Text(
                        _proyecto.descripcion,
                        textScaleFactor: 1.2,
                        style: Theme.of(context).textTheme.caption,
                      ),
                      
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void onProjectClick(){
      if(_proyecto.link!=null)
       html.window.open(_proyecto.link, 'Diego-González'); 
    }

}