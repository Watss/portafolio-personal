import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:portafolio/config/temas.dart';
import 'package:portafolio/models/TemaProvider.dart';
import 'package:portafolio/pages/ProyectosPage.dart';
import 'package:portafolio/pages/SobreMiPage.dart';
import 'package:provider/provider.dart';


void main() => runApp(PortafolioApp());

class PortafolioApp extends StatefulWidget {
  PortafolioApp({Key key}) : super(key: key);

  @override
  _PortafolioAppState createState() => _PortafolioAppState();
}

class _PortafolioAppState extends State<PortafolioApp> {
  TemaProvider proveedorDeTema = new TemaProvider();

  @override
  void initState() { 
    super.initState();
    getTemaActual();
  }

  void getTemaActual() {
    proveedorDeTema.darkTheme = false;
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(create: (_) {
      return proveedorDeTema;
      },
      child: Consumer<TemaProvider>(
        builder: (BuildContext context,value,Widget child) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: proveedorDeTema.darkTheme ? temaOscuro(context) : temaClaro(context),
            home: HomePage(),
          );
        },
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _indexSeleccionado = 0;
  static List<Widget> tabWidgets = <Widget>[
    SobreMiPage(),
   
    ProyectosPage()
    //  BlogTab(),
    // ProjectsTab(),
  ];
  static List<String> titlesList = <String>[
    '',
    'Portafolio',
  ];
  void _cambiarTab(int index) {
    setState(() {
      _indexSeleccionado = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _proveedorDeTema = Provider.of<TemaProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: ElasticInRight(duration: Duration(seconds: 1),child: Text(titlesList[_indexSeleccionado],textAlign: TextAlign.center, style: TextStyle(fontSize: 25),),),
        actions: <Widget>[
            IconButton(
              icon: Icon( _proveedorDeTema.darkTheme ? FontAwesomeIcons.sun:FontAwesomeIcons.moon,size: 30,),
              onPressed: () => _proveedorDeTema.darkTheme = !_proveedorDeTema.darkTheme,
            )
          ],
      ),
      body: Center(
        child: tabWidgets.elementAt(_indexSeleccionado),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.recent_actors), title: Text('Sobre Mi')),
              
          BottomNavigationBarItem(
              icon: Icon(Icons.card_travel), title: Text('Portafolio')),
        ],
        currentIndex: _indexSeleccionado,
        onTap: (index) => _cambiarTab(index),
        selectedItemColor: Theme.of(context).accentColor,
      ),
    );
  }
}
