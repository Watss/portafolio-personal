import 'dart:html' as html;
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:portafolio/config/assets.dart';
import 'package:portafolio/config/constants.dart';

class SobreMiPage extends StatelessWidget {

  const SobreMiPage({Key key}) : super(key: key);
  

  _openLink(String link) {
    html.window.open(link,'Diego-Gonzalez');
  }
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: FadeInUp(
              duration: Duration(seconds: 1),
              child: Center(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                  radius: 120,
                  backgroundImage: Image.asset(Assets.perfil).image,
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Diego González Zapata',
                  textScaleFactor: 3,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Apasionado por el desarrollo web, \nConstantemente aprendiendo nuevas tecnologias.',
                  style: Theme.of(context).textTheme.caption,
                  textScaleFactor: 2,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    FlatButton.icon(
                        onPressed: () => _openLink(Constants.PROFILE_GITHUB),
                        icon: Icon(FontAwesomeIcons.github),
                        label: Text('Github')
                    ),
                    FlatButton.icon(
                        onPressed: () => _openLink(Constants.PROFILE_GITLAB),
                        icon: Icon(FontAwesomeIcons.gitlab),
                        label: Text('Gitlab')
                    ),
                    FlatButton.icon(
                        onPressed: () => _openLink(Constants.PROFILE_INSTAGRAM),
                        icon: Icon(FontAwesomeIcons.instagram),
                        label: Text('Instagram')
                    ),
                  ],
                ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  FlatButton.icon(
                        onPressed: () => _openLink(Constants.PROFILE_FACEBOOK),
                        icon: Icon(FontAwesomeIcons.facebook),
                        label: Text('Facebook')
                    ),
                    FlatButton.icon(
                        onPressed: () => _openLink(Constants.PROFILE_LINKEDIN),
                        icon: Icon(FontAwesomeIcons.linkedinIn),
                        label: Text('LinkedIn')
                    ),
                ],
              )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
