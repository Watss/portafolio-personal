import 'package:animated_text/animated_text.dart';
import 'package:flutter/material.dart';
import 'package:portafolio/config/proyectos.dart';
import 'package:portafolio/utilities/proyecto_widget.dart';
import 'package:portafolio/utilities/responsive_widget.dart';

class ProyectosPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CardProyecto(context: context);
  }
}

class CardProyecto extends StatelessWidget {
  const CardProyecto({
    Key key,
    @required this.context,
  }) : super(key: key);

  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return ResponsiveWidget(
      largeScreen: GridView.count(
          padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 32.0),
          crossAxisCount: 3,
          childAspectRatio: MediaQuery.of(context).size.width /
              (MediaQuery.of(context).size.height / 1.3),
          children: List.generate(
              proyectos.length, (index) => ProyectoWidget(proyectos[index], 0)),
        ),
      smallScreen: ListView.builder(
          itemCount: proyectos.length,
          itemBuilder: (context, index) => ProyectoWidget(
              proyectos[index], (index == proyectos.length - 1 ? 16.0 : 0))),
    );
  }
}